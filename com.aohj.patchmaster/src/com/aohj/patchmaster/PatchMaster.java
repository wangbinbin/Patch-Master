package com.aohj.patchmaster;

import java.text.SimpleDateFormat;

import org.eclipse.osgi.util.NLS;

public class PatchMaster extends NLS {
	public static final String ROOT = ".patchmaster";
	public static final String COPY_PATH = "copyPath.txt";
	public static final String PATCH_PATH = "patchPath.txt";
	public static final String CHARSET = "UTF-8";
	public static final String LINE_COMMENT = "#TODO";
	public static final String LINE_SEPARATOR = System
			.getProperty("line.separator");
	public static final String FILE_SEPARATOR = "/";
	public static final String PREFIX = "yyyyMMdd_HH_mm";
	public static final String SEPARATOR = "_";
	public static final String TODO = "TODO";
	public static final boolean COMPRESS = true;;

	public static String join(Object... objects) {
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < objects.length; i++) {
			if (i > 0)
				sb.append(SEPARATOR);
			sb.append(objects[i]);
		}
		return String.valueOf(sb);
	}

	public static String newPrefix() {
		return new SimpleDateFormat(PREFIX).format(System.currentTimeMillis());
	}

	public static String getProjectPatchName(String projectName) {
		return join(newPrefix(), projectName, TODO, Message.USER,
				Message.SUFFIX);
	}

	public static String getProjectRepatchName(String projectPatchName) {
		return PatchMaster.join(projectPatchName, Message.RESUFFIX);
	}
}