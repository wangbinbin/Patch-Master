package com.aohj.patchmaster;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import org.eclipse.core.resources.IContainer;
import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IFolder;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.Path;

public class Files {
	public static IResource[] members(IContainer container)
			throws CoreException {
		if (container == null)
			return new IResource[0];
		return container.members();
	}

	public static IFile getFile(IContainer parent, String child) {
		return parent.getFile(Path.fromPortableString(child));
	}

	public static IFolder getFolder(IContainer parent, String child)
			throws CoreException {
		return parent.getFolder(Path.fromPortableString(child));
	}

	public static boolean newFile(IFile iFile, File file)
			throws FileNotFoundException, CoreException {
		if (iFile.exists())
			iFile.delete(true, null);
		iFile.create(new FileInputStream(file), true, null);
		return true;
	}

	public static IFolder newFolder(IContainer parent, String child)
			throws CoreException {
		IFolder folder = parent.getFolder(Path.fromPortableString(child));
		if (!folder.exists())
			folder.create(true, true, null);
		return folder;
	}

	public static File toFile(IResource resource) {
		return resource.getLocation().toFile();
	}

	public static String getName(IResource resource) throws CoreException {
		if (resource == null)
			return null;
		return resource.getName();
	}

	public static IProject getProject(IResource resource) throws CoreException {
		if (resource == null)
			return null;
		return resource.getProject();
	}

	public static boolean exists(IResource resource) throws CoreException {
		if (resource == null)
			return false;
		return resource.exists();
	}

	public static boolean delete(IResource resource) {
		if (resource == null)
			return true;
		try {
			resource.delete(true, null);
		} catch (CoreException e) {
			return false;
		}
		return true;
	}

	public static void refreshLocal(IResource resource) throws CoreException {
		resource.refreshLocal(IResource.DEPTH_INFINITE, null);
	}

	public static String getAbsolutePath(IResource resource) {
		return resource.getLocation().toFile().getAbsolutePath();
	}

	public static String getProjectAbsolutePath(IResource resource) {
		return resource.getProject().getLocation().toFile().getAbsolutePath();
	}

	public static String getProjectRelativePath(IResource file) {
		return file.getProjectRelativePath().toString();
	}

	public static void write(IFile file, byte[] bytes) throws CoreException {
		InputStream in = new ByteArrayInputStream(bytes);
		if (file.exists()) {
			file.appendContents(in, true, true, null);
		} else {
			file.create(in, true, null);
		}
	}

	public static List<String> readAllLines(File file, String charset)
			throws IOException {
		List<String> result = new ArrayList<String>();
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				new FileInputStream(file), charset));
		for (;;) {
			String line = reader.readLine();
			if (line == null)
				break;
			result.add(line);
		}
		reader.close();
		return result;
	}

	public static File newZip(File file) throws IOException {
		String fileName = file.getName();
		int lastIndex = fileName.lastIndexOf(".");
		if (lastIndex != -1)
			fileName = fileName.substring(0, lastIndex);
		fileName = fileName + ".zip";
		File zip = new File(file.getParent(), fileName);
		ZipOutputStream out = new ZipOutputStream(new FileOutputStream(zip));
		write(out, file, null);
		out.close();
		return zip;
	}

	private static boolean write(ZipOutputStream out, File file, String parent)
			throws IOException {
		if (out == null || file == null || !file.exists())
			return false;
		if (parent == null)
			parent = "";
		if (file.isDirectory()) {
			parent += file.getName() + File.separator;
			for (File child : file.listFiles()) {
				write(out, child, parent);
			}
		} else {
			FileInputStream in = new FileInputStream(file);
			out.putNextEntry(new ZipEntry(parent + file.getName()));
			byte[] bytes = new byte[1024];
			int read;
			while ((read = in.read(bytes)) != -1) {
				out.write(bytes, 0, read);
			}
			in.close();
		}
		return true;
	}
}