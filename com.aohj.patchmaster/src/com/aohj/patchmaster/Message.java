package com.aohj.patchmaster;

import org.eclipse.osgi.util.NLS;

public class Message extends NLS {
	public static String COPY_PATH_TITLE = "Patch Master - Provided by Ao Hujun";
	public static String COPY_PATH_MESSAGE = "Copy Path Action was executed.";
	public static String CREATE_PATCH_TITLE = "Patch Master - Powered by Ao Hujun";
	public static String CREATE_PATCH_MESSAGE = "Create Patch Action was executed.";
	public static String SUFFIX = "patch";
	public static String RESUFFIX = "repatch";
	public static String USER = "YOURNAME";
	public static String NEED_MAVEN_PACKAGE = "please executive maven clean package";
	private static final String BUNDLE_NAME = "com.aohj.patchmaster.message";
	static {
		NLS.initializeMessages(BUNDLE_NAME, Message.class);
		if (System.getenv("PATCH_MASTER_USER") != null)
			USER = System.getenv("PATCH_MASTER_USER");
	}
}